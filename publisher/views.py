from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse 
from django.views.decorators.cache import cache_page 
from django.conf import settings 
from publisher.models import News 
from publisher.tasks import send_admin_notification

# Create your views here.
def news_list(request, current_count):
    """
        Responds to a jQuery Ajax request to send the latest list 
        of published News articles to the home page in JSON format.
    """
    news_articles = News.objects.filter(publish=True)
    if len(news_articles) == current_count:
        return redirect('publisher:home')
    else:
        return JsonResponse(list(news_articles.values()), safe=False)

@cache_page(60* 0.125)
def home(request):
    """
        The single public-facing page of the app.
    """
    default_article = News.objects.filter(publish=True)[0]
    context_data = {
        'default_article': default_article
    }
    return render(request, "publisher/news_list.html", context_data)

def publish_article(request, pk):
    """
    Called by the custom 'Publish' button in admin's News change page
    to publish a News article.
    """
    article = get_object_or_404(News, pk=pk)
    article.publish = True
    article.save()
    return redirect('/admin/publisher/news/')

def notify_admin_of_button_click(request):
    """
    View that calls a Celery task to initiate sending an email to admin
    to notify them of someone clicking on the 'Help' button.
    """
    subject = "A User Clicked on the 'Help' Button"
    message = """
    Hello Admin, \n 
        A User just clicked on the 'Help' button on the homepage.
        \n 
        Sincerely,\n
        Celery Mailer
    """
    from_email = settings.CELERY_TASKS_EMAIL 
    recipient_list = ['edgeops@mediaedge.com','cto@mediaedge.com','techlead@mediaedge.com']
    send_admin_notification.delay(
        subject, message,
        from_email, recipient_list
    )
    return redirect('publisher:home')