from django.db import models
from django.utils.text import slugify 
from django.utils import timezone 
from django.contrib.auth.models import User 

# Create your models here.
class News(models.Model):
    """
    Represents a single news article on the MediaEdge's publisher app.
    """
    title = models.CharField(max_length=150)
    slug = models.SlugField(max_length=170, blank=True)
    body = models.TextField()
    author = models.ForeignKey(User, related_name="articles_written", on_delete=models.CASCADE)
    pub_date = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(editable=False, null=True)
    publish = models.BooleanField(default=False)

    class Meta:
        verbose_name = "News Article"
        verbose_name_plural = "News Articles"
        ordering = ['-last_updated','-pub_date',]

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        if self.pub_date:
            self.last_updated = timezone.now()
        super(News, self).save(*args, **kwargs)

    def __str__(self):
        return "%s by %s " % (self.title, self.author.username)


