from __future__ import absolute_import, unicode_literals
from celery import shared_task
from celery.utils.log import get_task_logger 
from django.core.mail import send_mail 

logger = get_task_logger(__name__)

@shared_task(name="Notify Admin of Button Click")
def send_admin_notification(subject, message, from_email, recipient_list):
    """
    Notify Admin of 'Help' Button Click.
    """
    logger.info("Notifying Admin of 'Help' Button Click.")
    return send_mail(subject, message, from_email, recipient_list, fail_silently=False)

