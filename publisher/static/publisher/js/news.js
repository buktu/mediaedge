$(document).ready(function(){
    console.log("MediaEdge Platform! :)")
})

function getUpdatedNewsList(){
    article_list_container = $('#article_list');
    let currentArticleCount = $('#article-count').text();
    $.ajax({
        url: '/news-list/' + currentArticleCount,
        type: 'get',
        success: function(response){
            article_list_container.empty();
            $.each(response, function(i,article){
                //console.log(article);
                article_list_container.append(`
                <div class='container'>
                    <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-2 news_article">
                        <h4 class="text-center article_title">
                            ${article.title }
                        </h4>
                        
                        <div class='article_body'> ${article.body}</div>
                    </div>
                </div>
                `)
            })
        }
    })

}


$(document).ready(function(){
    $('#article-count').hide();
    setInterval("getUpdatedNewsList()", 7000)
})
