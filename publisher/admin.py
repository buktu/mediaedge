from django.contrib import admin
from .models import News 
# Register your models here.

admin.ModelAdmin.change_form_template = "admin/publisher/news/change_form.html"

admin.site.register(News, admin.ModelAdmin)
