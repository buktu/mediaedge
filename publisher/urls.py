from django.urls import path 
from publisher import views 
from django.conf import settings 
from django.contrib.staticfiles.urls import staticfiles_urlpatterns 

app_name = "publisher"
urlpatterns = [
    path('news-list/<int:current_count>/', views.news_list, name="news_list"),
    path('publish/<int:pk>/', views.publish_article, name="publish-article"),
    path('notify-admin/', views.notify_admin_of_button_click, name="celery_notify_admin"),
    path('', views.home, name="home")
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()